<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Asistente extends Model
{
    protected $fillable = [
        'dni','apellidos','nombres','institucion'
    ];
}
