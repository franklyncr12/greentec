<?php

namespace App\Http\Controllers;

use App\Asistente;
use App\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use GuzzleHttp\Client;

class AsistenteController extends Controller
{
    /**
     * Display a listing of the resource.
     *asdf
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


        $asistentes = Asistente::latest()->paginate(5);

        return view('asistentes.index',compact('asistentes'))
            ->with('i', (request()->input('page', 1) - 1) * 5);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('asistentes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
	

        $request->validate([
            'dni' => 'required',
            'apellidos' => 'required',
            'nombres' => 'required',
            'institucion' => 'required',
        ]);

        $asistente = new Asistente();
        $asistente->dni = request('dni');
        $asistente->apellidos = request('apellidos');
        $asistente->nombres = request('nombres');
        $asistente->institucion = request('institucion');

        $path = '';
        if ($request->hasFile('foto')) {
            $foto_name = $request->dni.'_'.time();
            $path = $request->file('foto')->storeAs('public', $foto_name.'.jpg');
        }
        $websiteURl = env('APP_URL')."/pdf";
        $asistente->foto = $path;
        $asistente->fecha_emision = request('fecha_emision');

        $url = asset(Storage::url("".$path));
        
        $pdfData = array('apellidos' => request('apellidos'), 'nombres' => request('nombres'), "institucion" => request('institucion'), "path" => $url, "website_url" => $websiteURl ,"fecha_emision" => date("d-m-Y", strtotime(request('fecha_emision'))));
        

       $url =  env('APP_URL')."/pdf/index.php";
	   $method = 'POST';
	   $headers = array("content-type: application/json");
       $body = json_encode($pdfData);
        
        $curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_URL => $url,
			CURLOPT_CUSTOMREQUEST => $method,
			CURLOPT_HTTPHEADER => $headers,
			CURLOPT_POSTFIELDS => $body
		));

            
		$responseC = curl_exec($curl);
		$err = curl_error($curl);

        curl_close($curl);
            
		if ($err) {
                
			$req['error'] = false;
			$req['error_message'] = "cURL Error #:" .$err;
			$responseData[] = $req;
		} else {
                
            $responseData = json_decode($responseC,true);
            if($responseData['status'] == 'succeeded'){                 
                header('Content-Type: application/pdf');
                header('Content-disposition: attachment;filename=MyFile.pdf');
                readfile($responseData['url']);
                $asistente->save();
                return redirect()->route('asistentes.create')->with('success','Imprimiendo');
            }
		}
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Asistente  $asistente
     * @return \Illuminate\Http\Response
     */
    public function show(Asistente $asistente)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Asistente  $asistente
     * @return \Illuminate\Http\Response
     */
    public function edit(Asistente $asistente)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Asistente  $asistente
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Asistente $asistente)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Asistente  $asistente
     * @return \Illuminate\Http\Response
     */
    public function destroy(Asistente $asistente)
    {
        //
    }

    public function reniec(Request $request)
    {
       $dni = $request->input('dni');
       $client = new Client([
        'base_uri' => 'https://api.apirest.pe'
		]);
		// reniec corregido
		 if (strlen($dni)== 8) {
			   //franklyncr12
			$response = $client->request('POST', 'api/getDniPremium', [
				'headers' => [
					'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImU3ZDNlMDNhNjdkMDRmNTJhOTM1M2Y1YTViNTRiNjdlZWM1Y2FlNjU3NGNhNDc0OWFkNzNiOWM1OWYyMzljOTA2M2IxMzZhZjVmNTMyNjk2In0.eyJhdWQiOiIxIiwianRpIjoiZTdkM2UwM2E2N2QwNGY1MmE5MzUzZjVhNWI1NGI2N2VlYzVjYWU2NTc0Y2E0NzQ5YWQ3M2I5YzU5ZjIzOWM5MDYzYjEzNmFmNWY1MzI2OTYiLCJpYXQiOjE1NjgxNTIzNDksIm5iZiI6MTU2ODE1MjM0OSwiZXhwIjoxODgzNzcxNTQ5LCJzdWIiOiIxOTgiLCJzY29wZXMiOlsiKiJdfQ.C3gqwb1ouwL0VPXfbbpP3X_SmkDJhGuug4sZIrI2oqETnlY1zcHlkaILK3rHLgcW_hyqUMaCsYfsXC8UEbmG8eFgczcCY_o2ezwVtP0-kejbYR9oEm-MnrkX_G1WJUtmvQL-LBNfzbZhrTecDjMFdk2OGNwcak52Xg0tgAaCz1QPMls5LH8tezbNb1ie5GNaP0VPL5b3ayl3CQhUfdGkJKuHhiQWVVCGRVDOIJi01mIYrml39ngsqzRk7R445S9vpl77MWOkMdBJGY7N2sU6JaQXbu_CY8CkE6Ez6NY56dciekuGfdYyzD2HeabK55P_9L6aXj4VOhba5NWZdEpw_WQx_04jOy600UIqliO9oRkqw5emtNH0LGus5hYjDo5Czw5aPgHWaU7SKrPO3vNFhS6waWckvuIoY60PiHaLCDndmcMbDYMTEuNZWN6SLh4P_Wz1mhgscxJYwhtUbHhQZu_r9CFFQdnBxBU1eK1pYoYye2ft18uExttbZynOEXMjl2XbTdwS6a3jk6YYKKTvfizgfVhLPtxv9ocK9B_I6AHGgOu-zsQwxYyTx2ieYqBAqxw_QvT9WjZpFOUC7FKkpZBivrL-CunvxMZfyqHsdBDG_cMUXjDub43yQcEFtrPrsJXH1t-qzRAjhj00dCPbYMnzN_Us6uOAt-b9Sx6kt1o'
				],
				'json' => [
					'dni' => $dni
				]
			]);
		}

		return $response = $response->getBody()->getContents();
	}
}
