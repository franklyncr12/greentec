<?php
use setasign\Fpdi\Fpdi;
use setasign\Fpdi\PdfReader;

require_once('fpdf/fpdf.php');
require_once('fpdi/src/autoload.php');


$data = json_decode(file_get_contents('php://input'), true);

// initiate FPDI
$pdf = new Fpdi();
// set the source file
$pdf->setSourceFile('stickerpasaporte2019color.pdf');
// import page 1

$pageId = $pdf->importPage(1, PdfReader\PageBoundaries::MEDIA_BOX);
$pdf->addPage('L','A4');
$pdf->useImportedPage($pageId, 0, 0);
// Add Montserrat Font Regular
$pdf->AddFont('Montserrat-Regular','','Montserrat-Regular.php');

// Add Montserrat Font Bold
$pdf->AddFont('Montserrat-Bold','','Montserrat-Bold.php');

// Add Montserrat Font Light
$pdf->AddFont('Montserrat-Light','','Montserrat-Light.php');

// Need to customization below:


$pdf->SetFont('Montserrat-Light', '',8);
$pdf->SetTextColor(0, 0, 0);

//===== First LineLogic
$pdf->SetXY(3.2, 84);
$pdf->Cell(0,8,$data['apellidos'],0,0,'L');

//===== Second LineLogic
$pdf->SetXY(3.2, 84 + 8.5);
$pdf->Cell(0,8,$data['nombres'],0,0,'L');


//=====Third  LineLogic
$pdf->SetXY(3.2, 84 + 8 + 8 + 0.5);
$pdf->Cell(0,8,$data['institucion'],0,0,'L');


//=====Forth  LineLogic
$pdf->SetXY(3.2, 84 + 8 + 8 + 8 + 1);
$pdf->Cell(0,8,$data['fecha_emision'],0,0,'L');


$websiteURL = $data['path']; // Dynamic URL
//$websiteURL = "https://my-cpe.com/avatars/speaker/1566393587Profile-Pic.jpg"; // Live URL
$pdf->Image($websiteURL,90.7,75,22,0,'JPG');
//C:\xampp\htdocs\website\storage\app\public\74170429_1568548402.jpg


$filename="pdf/".time().".pdf";
$pdf->Output('F',$filename);
$filePath = $data['website_url']."/".$filename;
echo json_encode(array("status"=>"succeeded","url"=>$filePath));

