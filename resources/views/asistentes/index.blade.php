@extends('asistentes.layout')
 
@section('content')

<div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title ">Asistentes</h4>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table">
                      <thead class=" text-primary">
                        <th>
                          DNI
                        </th>
                        <th>
                          Apellidos
                        </th>
                        <th>
                          Nombres
                        </th>
                        <th>
                          Institución
                        </th>
                        <th>
                          Fecha
                        </th>
                      </thead>
                      <tbody>
                      @foreach ($asistentes as $asistente)
                        <tr>
                          <td>
                          {{  $asistente->dni }}
                          </td>
                          <td>
                          {{  $asistente->apellidos }}
                          </td>
                          <td>
                          {{  $asistente->nombres }}
                          </td>
                          <td>
                          {{  $asistente->institucion }}
                          </td>
                          <td class="text-primary">
                          {{  $asistente->fecha_emision }}
                          </td>
                        </tr> 
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

@endsection