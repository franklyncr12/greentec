@extends('asistentes.layout')
 
@section('content')

<form enctype="multipart/form-data" action="{{ route('asistentes.store') }}" method="POST" accept-charset="utf-8">
@csrf
<div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-8">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title">Asistente</h4>
                </div>
                <div class="card-body">
                  <form>
                    <div class="row">
                      <div class="col-md-8">
                        <div class="form-group">
                          <label class="bmd-label-floating">DNI</label>
                          <input type="text" id="dni" name="dni" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-4">
                        <button id="rest" class="btn btn-primary">Buscar</button>
                  
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Apellidos</label>
                          <input type="text" name="apellidos" class="form-control">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Nombres</label>
                          <input type="text" name="nombres" class="form-control">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Institución</label>
                          <input type="text" name="institucion" class="form-control">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <input type="date" id="fecha_emision" name="fecha_emision" class="form-control">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <input type="file" name="foto" name="userfile"></input>
                      </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Registrar</button>
                    <div class="clearfix"></div>
                  </form>
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>

      
      
      </form>
      @endsection