<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Storage;

Route::get('/', function () {
    return view('welcome');
});

Route::resource('products','ProductController');

Route::resource('asistentes','AsistenteController');


Route::get('/reniec', 'AsistenteController@reniec');


Route::get('ajaxRequest', 'HomeController@ajaxRequest');
Route::post('ajaxRequest', 'HomeController@ajaxRequestPost');


Route::get('storage/{filename}', function ($filename)
{
    $path = storage_path('public/' . $filename);
 
    if (!File::exists($path)) {
        abort(404);
    }
 
    $file = File::get($path);
    $type = File::mimeType($path);
 
    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);
 
    return $response;
});

Route::post('process', function (Request $request) {
    // cache the file
    $file = $request->file('photo');
 
    // generate a new filename. getClientOriginalExtension() for the file extension
    $filename = 'profile-photo-' . time() . '.' . $file->getClientOriginalExtension();
 
    // save to storage/app/photos as the new $filename
    $path = $file->storeAs('photos', $filename);
 
    dd($path);
});